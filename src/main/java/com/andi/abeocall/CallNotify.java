package com.andi.abeocall;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class CallNotify implements Runnable{
	
	private AbeoCall plugin;
	
	public CallNotify(AbeoCall plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(p.hasPermission("abeocall.mod")) {
				int amount = plugin.getHandleCallData().getHashMap().size();
				
				if(amount == 0)
					continue;
				p.sendMessage(AbeoCall.mh + " " + ChatColor.GRAY + amount + " calls are open. Use '/call list' to view them");
			}
		}
	}

}
