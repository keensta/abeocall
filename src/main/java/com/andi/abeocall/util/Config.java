package com.andi.abeocall.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.andi.abeocall.AbeoCall;

public class Config {

	public AbeoCall plugin;
	Logger log = Logger.getLogger("Minecraft");

	public Config(AbeoCall plugin) {
		this.plugin = plugin;
	}

	public void checkConfig(String name) {
		File dataDirectory = plugin.getDataFolder();
		if(dataDirectory.exists() == false)
			dataDirectory.mkdirs();

		File f = new File(plugin.getDataFolder(), name + ".yml");
		if(f.canRead())
			return;

		log.info("[AbeoCall] Writing default " + name + ".yml");
		InputStream in = plugin.getResource(name + ".yml");

		if(in == null) {
			log.severe("[AbeoCall] Could not find " + name + ".yml" + " resource");
			return;
		} else {
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(f);
				final byte[] buf = new byte[512];
				int len;
				while ((len = in.read(buf)) > 0) {
					fos.write(buf, 0, len);
				}
			} catch (IOException iox) {
				log.severe("[AbeoCall] Could not export " + name + ".yml");
				return;
			} finally {
				if(fos != null)
					try {
						fos.close();
					} catch (final IOException iox) {
					}
				if(in != null)
					try {
						in.close();
					} catch (final IOException iox) {
					}
			}
			return;
		}
	}

	public FileConfiguration loadConfig(String name) {
		return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), name + ".yml"));
	}

	public void saveConfig(FileConfiguration cfg, String name) {
		try {
			cfg.save(new File(plugin.getDataFolder(), name + ".yml"));
		} catch (IOException e) {
			Logger.getLogger("Minecraft").severe("[AbeoCall] Failed to save " + name + ".yml!");
			e.printStackTrace();
		}
	}

}
