package com.andi.abeocall;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.andi.abeocall.commands.CloseCommand;
import com.andi.abeocall.commands.CommandHandler;
import com.andi.abeocall.commands.CreateCommand;
import com.andi.abeocall.commands.ListCommand;
import com.andi.abeocall.commands.WarpCommand;
import com.andi.abeocall.data.HandleCallData;
import com.andi.abeocall.util.Config;

public class AbeoCall extends JavaPlugin {
	
	Logger log = Logger.getLogger("Minecraft");
	
	public Config configManager = null;
	public CommandHandler handler = null;
	public HandleCallData handleCD = null;
	public CallNotify callNotify = null;
	
	public static String mh = "[AbeoCall]";
	
	@Override
	public void onEnable() {
		configManager = new Config(this);
		handler = new CommandHandler(this);
		handleCD = new HandleCallData(this);
		callNotify = new CallNotify(this);
		
		getCommand("call").setExecutor(handler);
		loadCommands();
		
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, callNotify, 6000, 6000);
		
		log.info(mh + " AbeoCall has been enabled ");
	}

	@Override
	public void onDisable() {
		log.info(mh + " AbeoCall has been disabled");
	}
	
	
	private void loadCommands() {
		handler.registerCommand(new CreateCommand());
		handler.registerCommand(new CloseCommand());
		handler.registerCommand(new ListCommand());
		handler.registerCommand(new WarpCommand());
	}
	
	
	public Config getConfigManager() {
		return configManager;
	}
	
	public HandleCallData getHandleCallData() {
		return handleCD;
	}
}
