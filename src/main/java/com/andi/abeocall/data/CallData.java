package com.andi.abeocall.data;

import org.bukkit.Location;

public class CallData {
	
	private int id = 0;
	private String creator = "";
	private String message = "";
	private Location openLoc = null;
	
	public CallData(int id, String creator, String message, Location openLoc) {
		this.id = id;
		this.creator = creator;
		this.message = message;
		this.openLoc = openLoc;
	}
	
	public String getMessage() {
		return message;
	}
	
	public int getId() {
		return id;
	}
	
	public String getCreator() {
		return creator;
	}
	
	public Location getLocation() {
		return openLoc;
	}
	
	public String getLocationString() {
		String sOpenLoc = openLoc.getBlockX() +"," + openLoc.getBlockY() +","+ openLoc.getBlockZ();
		return sOpenLoc;
	}

}
