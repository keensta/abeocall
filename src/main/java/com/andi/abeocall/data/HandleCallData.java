package com.andi.abeocall.data;

import java.util.HashMap;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.andi.abeocall.AbeoCall;

public class HandleCallData {

	private AbeoCall plugin;
	private Logger log = Logger.getLogger("Minecraft");
	private HashMap<String, CallData> openCalls = new HashMap<String, CallData>();
	private int currentId = 0;

	public HandleCallData(AbeoCall plugin) {
		this.plugin = plugin;
		loadData();
	}

	public void loadData() {
		FileConfiguration cfg = plugin.getConfigManager().loadConfig("CallData");
		openCalls.clear();

		if(cfg == null) {
			log.info(AbeoCall.mh + " Unable to find 'CallData.yml'");
			return;
		}

		if(!cfg.contains("calls") || !cfg.isConfigurationSection("calls"))
			return; // Nothing to load
 
		Set<String> calls = cfg.getConfigurationSection("calls").getKeys(false);

		if(cfg.contains("currentID")) {
			setCurrentId(cfg.getInt("currentID"));
		} else {
			setCurrentId(0);
		}

		for(String id : calls) {
			ConfigurationSection callEntry = cfg.getConfigurationSection("calls." + id);

			World worldName = Bukkit.getWorld(callEntry.getString("openLocWorld"));
			String[] locS = callEntry.getString("openLoc").split(",");
			Location openLoc = new Location(worldName, Integer.parseInt(locS[0]), Integer.parseInt(locS[1]), Integer.parseInt(locS[2]));

			CallData cd = new CallData(Integer.parseInt(id), callEntry.getString("creator"), callEntry.getString("message"), openLoc);
			openCalls.put(id, cd);
		}
	}

	public void saveData(CallData cd) {
		FileConfiguration cfg = plugin.getConfigManager().loadConfig("CallData");

		cfg.createSection("calls." + cd.getId());
		ConfigurationSection callEntry = cfg.getConfigurationSection("calls." + cd.getId());
		callEntry.set("openLocWorld", cd.getLocation().getWorld().getName());
		callEntry.set("openLoc", cd.getLocationString());
		callEntry.set("creator", cd.getCreator());
		callEntry.set("message", cd.getMessage());

		plugin.getConfigManager().saveConfig(cfg, "CallData");
		openCalls.put(String.valueOf(cd.getId()), cd);
	}

	public boolean deleteData(CallData cd, CommandSender sender, String reason) {
		FileConfiguration cfg = plugin.getConfigManager().loadConfig("CallData");

		if(cfg == null) {
			log.severe(AbeoCall.mh + " Unable to find 'CallData.yml' Please Reload the server!");
			log.severe(AbeoCall.mh + " If this error continues contact Developer");
			return false;
		}

		if(cfg.contains("calls." + cd.getId())) {
			cfg.set("calls." + cd.getId(), null);
			removeCallData(cd.getId());
			plugin.getConfigManager().saveConfig(cfg, "CallData");
			for(Player p : Bukkit.getOnlinePlayers()) {
				if(p.getName().equalsIgnoreCase(sender.getName()))
					continue;
				else {
					if(p.hasPermission("abeocall.mod")) {
						p.sendMessage(AbeoCall.mh + ChatColor.GRAY + " Call " + cd.getId() + " has been closed. Reason: " + (reason.length() == 0 ? "N/A" : reason));
					}
				}
			}
			return true;
		}
		return false;
	}

	public boolean containsCD(int id) {
		return openCalls.containsKey(String.valueOf(id));
	}

	public CallData getCallData(int id) {
		return openCalls.get(String.valueOf(id));
	}

	public void removeCallData(int id) {
		openCalls.remove(String.valueOf(id));
	}

	public HashMap<String, CallData> getHashMap() {
		return openCalls;
	}

	public int getCurrentId() {
		return currentId;
	}

	public void setCurrentId(int currentId) {
		this.currentId = currentId;
		FileConfiguration cfg = plugin.getConfigManager().loadConfig("CallData");
		cfg.set("currentID", this.currentId);
		plugin.getConfigManager().saveConfig(cfg, "CallData");
	}
}
