package com.andi.abeocall.commands;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.andi.abeocall.AbeoCall;
import com.andi.abeocall.data.CallData;

public class ListCommand implements CallCommand{

	@Override
	public String getCommand() {
		return "List";
	}

	@Override
	public void help(CommandSender sender) {
		sender.sendMessage(AbeoCall.mh + ChatColor.BLUE + " Usage /call list");
	}

	@Override
	public void processCommmand(CommandSender sender, AbeoCall plugin, String[] args) {
		HashMap<String, CallData> callData = plugin.handleCD.getHashMap();
		
		if(callData.size() == 0) {
		    sender.sendMessage(AbeoCall.mh + ChatColor.GRAY + " No calls have been made, seems too quiet.");
		    return;
		}
		
		for(String cdS : callData.keySet()) {
			CallData cd = callData.get(cdS);
			sender.sendMessage(ChatColor.GOLD + "[" + ChatColor.GRAY + cd.getId() + ChatColor.GOLD + "]" + ChatColor.GRAY + cd.getCreator()
					+ " : " + cd.getMessage());
		}
		
	}

	
	
}
