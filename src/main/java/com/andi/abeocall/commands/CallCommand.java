package com.andi.abeocall.commands;

import org.bukkit.command.CommandSender;

import com.andi.abeocall.AbeoCall;

public interface CallCommand {
	 public String getCommand();
     public void help(CommandSender sender);
     public void processCommmand(CommandSender sender, AbeoCall plugin, String args[]);
}
