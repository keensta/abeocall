package com.andi.abeocall.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.andi.abeocall.AbeoCall;
import com.andi.abeocall.data.CallData;

public class CreateCommand implements CallCommand {

	@Override
	public String getCommand() {
		return "create";
	}

	@Override
	public void help(CommandSender sender) {
		sender.sendMessage(AbeoCall.mh + ChatColor.BLUE + " Usage '/call create [Message]'");
	}

	@Override
	public void processCommmand(CommandSender sender, AbeoCall plugin, String[] args) {
		
		if(args.length == 0) {
			help(sender);
		} else {
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < args.length; i++) {
				sb.append(args[i]);
				sb.append(" ");
			}
			String message = sb.toString().trim();
			Location openLoc = ((Player)sender).getLocation();
			int id = plugin.getHandleCallData().getCurrentId() + 1;
			CallData cd = new CallData(id, sender.getName(), message, openLoc);
			
			plugin.getHandleCallData().setCurrentId(id);
			plugin.getHandleCallData().saveData(cd);
			
			sender.sendMessage(AbeoCall.mh + ChatColor.GREEN + " Call created. Your call id is " + id);
			sender.sendMessage(ChatColor.GRAY + "Call Message: " + message);
		}
		
	}

}
