package com.andi.abeocall.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.andi.abeocall.AbeoCall;
import com.andi.abeocall.data.CallData;

public class WarpCommand implements CallCommand {

	@Override
	public String getCommand() {
		return "warp";
	}

	@Override
	public void help(CommandSender sender) {
		sender.sendMessage(AbeoCall.mh + ChatColor.BLUE + " Usage /call warp [id]");
	}

	@Override
	public void processCommmand(CommandSender sender, AbeoCall plugin, String[] args) {
		
		if(args.length == 0) {
			help(sender);	
		} else {
			
			if(sender.hasPermission("abeocall.mod")) {
				int id = 0;
				id = getInt(args[0]);
				
				if(plugin.getHandleCallData().containsCD(id)) {
					CallData cd = plugin.getHandleCallData().getCallData(id);
					
					((Player)sender).teleport(cd.getLocation());
					sender.sendMessage(AbeoCall.mh + ChatColor.GRAY + " Warped to " + id);
				} else {
					sender.sendMessage(AbeoCall.mh + ChatColor.RED + " " + id + " Does not exist.");
				}
				
			} else {
				sender.sendMessage(AbeoCall.mh + ChatColor.RED + " You don't have permission");
			}
			
		}

	}
	
	private int getInt(String string) {
		try {
			return Integer.parseInt(string);
		} catch (Exception e) {
			return -1;
		}
	}
}
