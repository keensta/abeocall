package com.andi.abeocall.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.andi.abeocall.AbeoCall;
import com.andi.abeocall.data.CallData;

public class CloseCommand implements CallCommand {

	@Override
	public String getCommand() {
		return "Close";
	}

	@Override
	public void help(CommandSender sender) {
		sender.sendMessage(AbeoCall.mh + ChatColor.BLUE + " Usage '/call close [id] {reason}");
	}

	@Override
	public void processCommmand(CommandSender sender, AbeoCall plugin, String[] args) {
		if(args.length == 0) {
			help(sender);
		} else {	
			int id = 0;
			id = getInt(args[0]);
			
			if(!plugin.handleCD.containsCD(id)) {
				sender.sendMessage(AbeoCall.mh + ChatColor.RED + " Seems this Id is not in use.");
				return;
			} else {
				
				CallData cd = plugin.handleCD.getCallData(id);
				String reason = (args.length > 2 ? getReason(args) : "");
				
				if(!sender.hasPermission("abeocall.mod") && !sender.getName().equalsIgnoreCase(cd.getCreator())) {
					sender.sendMessage(AbeoCall.mh + ChatColor.RED + " Only a mod or the call creator can close a call");
					return;
				}
				
				
				if(plugin.handleCD.deleteData(cd, sender, reason)) {
					sender.sendMessage(AbeoCall.mh + ChatColor.GRAY + " Call " + id + " has been closed " + (reason.length() == 0 ? "" : "Reason: " + reason));
					return;
				} else {
					sender.sendMessage(AbeoCall.mh + ChatColor.RED + " Failed to delete " + id);
				}
			}

		}
	}

	private String getReason(String[] args) {
		StringBuilder sb = new StringBuilder();
		for(int i = 1; i < args.length; i++) {
			sb.append(args[i]);
			sb.append(" ");
		}
		return sb.toString().trim();
	}

	private int getInt(String string) {
		try {
			return Integer.parseInt(string);
		} catch (Exception e) {
			return -1;
		}
	}

}
