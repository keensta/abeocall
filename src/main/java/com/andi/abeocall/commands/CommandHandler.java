package com.andi.abeocall.commands;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.andi.abeocall.AbeoCall;

public class CommandHandler implements CommandExecutor {
	
	private AbeoCall plugin;
	
	public CommandHandler(AbeoCall plugin) {
		this.plugin = plugin;
	}
	
	private Map<String, CallCommand> commands = new TreeMap<String, CallCommand>();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("call")) {
			
			if (!(sender instanceof Player))
                return true;    
        
			if (args.length == 0) {
				sender.sendMessage(AbeoCall.mh + ChatColor.RED + " Use /call help for more information");
        	} else {
    			String command = args[0].toLowerCase();
    			
        		if(command.equalsIgnoreCase("help")) {
        			
        			if(args.length < 2) {
        				sender.sendMessage(ChatColor.BLUE + "Avalible Commands:");
        				
        				StringBuilder sb = new StringBuilder();
        				for(String command1 : commands.keySet()) {
        					 sb.append(command1);
        					 sb.append(", ");
        				}
        				
        				sender.sendMessage(ChatColor.WHITE + sb.toString());
        				sender.sendMessage(ChatColor.BLUE + "Use '/call help <Command>' for more info about the specified command");
        				
        			} else {
        				CallCommand Command = commands.get(args[1]);
        				
        				if(Command == null) {
        					sender.sendMessage(AbeoCall.mh + ChatColor.RED + " Unknown command '"+args[1]+"'");
        				} else {
        					Command.help(sender);
        				}
        				
        			}
        		} else if(commands.containsKey(command)) {
        			String[] cmdArgs = Arrays.copyOfRange(args, 1, args.length);        
                    commands.get(command).processCommmand(sender, plugin, cmdArgs);
        		} else {
        			sender.sendMessage(AbeoCall.mh + ChatColor.RED + "Unknown command '"+args[0]+"'");
        		}
			}
		
		}
		return false;
	}
	
	public void registerCommand(CallCommand command) {
        commands.put(command.getCommand().toLowerCase(), command);
	}
}
